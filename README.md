# fa1.2

An implementation of an FA1.2 contract following the tzip-7 standard.

The compiled output of LIGO is located in two formats: 
[fa1.2.tz](./ligo/contracts/fa1.2.tz) and [fa1.2.json](./ligo/contracts/fa1.2.json).

If you want to compile the LIGO code yourself, make sure you have `ligo` and
`dos2unix` installed, run `chmod u+x compile.sh` then `./compile.sh`. If there 
are any errors then it is possible something has changed in the ligo compiler. The
hash of the version used to compile the contract is annotated in [compile.sh](./ligo/compile.sh).

For details on how to originate and call the entrypoints of this FA1.2 contract,
read the document [fa1.2-cli.md](./docs/fa1.2-cli.md).
