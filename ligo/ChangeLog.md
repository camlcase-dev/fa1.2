## 2020-09-22 -- v0.5.0.0
* Fix getAllowance. It was implemented incorrectly.

## 2020-09-04 -- v0.4.0.0
* Conform to tzip-7 expectations about approve entrypoint behavior. If allowance
  is zero it can be set to non-zero. If allowance is non-zero it can only be set
  to zero.

## 2020-07-15 -- v0.3.0.0
* Allow approve to approve owner even though they it doesn't affect what they
  can spendit. This might be useful for apps that generically use the approval
  entrypoint without considering who the approve spender is.
* Remove the metadata version of fa1.2.ligo.
* Recompile with latest ligo.

## 2020-07-01 -- v0.2.0.0
* Two version of fa1.2.ligo with and without metadata because metadata is not
  standardized in the fa1.2 specification (it only references fa2's standard
  which does not match up 100% to fa1.2's design).
* Recompile with latest ligo.

## 2020-07-01 -- v0.1.0.0

* Move fa1.2.ligo and related files from the camlCase dexter repoistory to its own.
* Add the token_metadata to storage.
* Reorganize storage to force the big_map to the upper left of the storage.
* Rename store to storage.
