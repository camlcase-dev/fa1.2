{-|
Module      : FaOnePointTwo.Contract
Copyright   : (c) camlCase, 2020

-- https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md
-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE TypeSynonymInstances      #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module FaOnePointTwo.Contract where

import Lorentz

--
-- | The token owner in an FA1.2 contract.
type Owner      = ("owner"     :! Address)

-- | The token receiver in the transfer entrypoint.
type Receiver   = ("receiver"  :! Address)

-- | An address that may have rights to spend FA1.2 token on behalf of the owner.
type Spender    = ("spender"   :! Address)

-- =============================================================================
-- Entrypoint Parameters
-- =============================================================================

type ApproveParams =
  ( "spender" :! Address
  , "value"   :! Natural
  )

-- (address :from, (address :to, nat :value))    %transfer
type TransferParams =
  ( "owner" :! Owner
  , "to"    :! Receiver
  , "value" :! Natural
  )

--  ((address :owner, address :spender), contract nat) %getAllowance
-- this forces owner and spender to be paired, then pair with contract to match
-- the specification
type OwnerSpender =
  ( "owner"   :! Owner
  , "spender" :! Address
  )

type GetAllowanceParams =
  ( "ownerSpender" :! OwnerSpender
  , "contr"   :! ContractRef Natural
  )

-- ((address :owner), contract nat)              %getBalance
type GetBalanceParams =
  ( "owner" :! Owner
  , "contr" :! ContractRef Natural
  )

type GetTotalSupplyParams =
  ( "unit"  :! ()
  , "contr" :! ContractRef Natural
  )

-- =============================================================================
-- Parameters
-- =============================================================================

data Parameter
  = Approve        ApproveParams    
  | Transfer       TransferParams
  | GetAllowance   GetAllowanceParams
  | GetBalance     GetBalanceParams
  | GetTotalSupply GetTotalSupplyParams
  deriving (Generic, IsoValue, HasAnnotation)

instance ParameterHasEntrypoints Parameter where
  type ParameterEntrypointsDerivation Parameter = EpdPlain

-- =============================================================================
-- Storage
-- =============================================================================

data StorageFields =
  StorageFields
    { totalSupply  :: Natural
    } deriving (Generic, IsoValue, Eq, Show, HasAnnotation)

instance HasFieldOfType StorageFields name field => StoreHasField StorageFields name field where
  storeFieldOps = storeFieldOpsADT

instance TypeHasDoc StorageFields where
  typeDocMdDescription = "FA1.2 storage fields."
  typeDocHaskellRep = homomorphicTypeDocHaskellRep
  typeDocMichelsonRep = homomorphicTypeDocMichelsonRep

type Approvals = Map Address Natural

type Account =
  ( "balance"   :! Natural
  , "approvals" :! Approvals
  )

data Storage =
  Storage
    { accounts :: BigMap Address Account
    , fields   :: StorageFields
    } deriving (Generic, IsoValue, Eq, Show, HasAnnotation)

instance (StoreHasField StorageFields fname ftype) => StoreHasField Storage fname ftype where
  storeFieldOps = storeFieldOpsDeeper #fields

instance StoreHasSubmap Storage "accounts" Address Account where
  storeSubmapOps = storeSubmapOpsDeeper #accounts

instance TypeHasDoc Storage where
  typeDocMdDescription = "FA1.2 storage."
  typeDocHaskellRep = homomorphicTypeDocHaskellRep
  typeDocMichelsonRep = homomorphicTypeDocMichelsonRep


-- =============================================================================
-- Contract Code
-- =============================================================================

contractCode :: ContractCode Parameter Storage
contractCode = do
  unpair
  entryCaseSimple @Parameter
    ( #cApprove        /-> approve
    , #cTransfer       /-> transfer
    , #cGetAllowance   /-> getAllowance
    , #cGetBalance     /-> getBalance
    , #cGetTotalSupply /-> getTotalSupply
    )

contract :: Contract Parameter Storage
contract =
  defaultContract $ contractName "fa1.2" $ do
    contractGeneralDefault
    docStorage @Storage
    -- doc $ DDescription contractDoc
    contractCode
  
approve :: Entrypoint ApproveParams Storage
approve = do
  getField #spender; sender; eq
  if_
    (do -- sender is spender
        drop
    )
    (do -- sender is not spender
        -- look up owner account with sender
        swap; getField #accounts; sender; get
        ifNone
          (do -- owner does not have an account, make one
              constructT @Account
                ( fieldCtor $ push @Natural 0 >> toNamed #balance
                , fieldCtor $ emptyMap >> toNamed #approvals
                )
          )
          nop

        -- if allowance is zero, allow update
        -- if not, require setting to zero
        stackType @[Account, Storage, ApproveParams]
        getField #approvals; duupX @4; toField #spender; get
        ifNone
          nop
          (do
              stackType @[Natural, Account, Storage, ApproveParams]
              int
              if IsZero
                then nop
                else do
                  duupX @3; toField #value; int; ifEq0 nop (do push @MText [mt|UnsafeAllowanceChange|]; failWith)
          )
        
        -- update spender's allownace for owner
        stackType @[Account, Storage, ApproveParams]
        getField #approvals; dig @3       
        
        getField #spender; dip (do toField #value; some)
        update; setField #approvals

        dip (do getField #accounts); some; sender; update
        setField #accounts
    )
  nil; pair

intToNatural :: MText -> Integer : s :-> Natural : s
intToNatural message = do
  dup; push @Integer 0; le; if_ abs (do push @MText message; failWith)

getOwnersAccount :: Owner : Storage : s :-> Account : Storage : s
getOwnersAccount = do
  fromNamed #owner; dip (do dup; toField #accounts); get
  ifNone (do push [mt|NotEnoughBalance|]; failWith) nop

transfer :: Entrypoint TransferParams Storage
transfer = do
  -- get owner's balance
  -- stackType @[TransferParams, Storage]
  getField #owner; dip swap; getOwnersAccount
  stackType @[Account, Storage, TransferParams]

  -- check if sender is the owner
  duupX @3; toField #owner; fromNamed #owner; sender; eq

  if_ (nop -- sender is owner
      )
      (do -- sender is not owner
          stackType @[Account, Storage, TransferParams]
          -- get the owner's approval
          getField #approvals; sender; get
          ifNone
            (do push [mt|NotEnoughAllowance|]; failWith)
            nop
          stackType @[Natural, Account, Storage, TransferParams]

          -- get and update spender's approval
          dip (do duupX @3; toField #value; dup);
          dup; dip (assertGe [mt|NotEnoughAllowance|])
          sub; intToNatural [mt|Allowance - value is negative. This should not happen.|];

          stackType @[Natural, Account, Storage, TransferParams]
          dip (getField #approvals); some; sender; update; setField #approvals
      )
  stackType @[Account, Storage, TransferParams]

  stackType @[Account, Storage, TransferParams]
  getField #balance; duupX @4; toField #value; assertLe [mt|NotEnoughBalance|]

  -- update owner balance
  stackType @[Account, Storage, TransferParams]
  duupX @3; toField #value; dip (getField #balance); swap; sub; intToNatural [mt|Owner's balance - value is negative. This should not happen.|]; setField #balance
  some; dip (getField #accounts); duupX @4; toField #owner; fromNamed #owner; update; setField #accounts;

  -- update to balance
  stackType @[Storage, TransferParams]
  -- get receiver from accounts
  getField #accounts; duupX @3; toField #to; fromNamed #receiver; get
  ifNone (do
             stackType @[Storage, TransferParams]
             swap
             constructT @Account
               ( fieldCtor $ getField #value >> toNamed #balance
               , fieldCtor $ emptyMap >> toNamed #approvals
               )
             stackType @[Account, TransferParams, Storage]
             some; swap; toField #to; fromNamed #receiver
             dip (dip (getField #accounts)); update; setField #accounts
         )
         (do
             stackType @[Account, Storage, TransferParams]
             getField #balance; duupX @4; toField #value; add; setField #balance
             stackType @[Account, Storage, TransferParams]
             some; dip (getField #accounts); dig @3; toField #to; fromNamed #receiver
             update; setField #accounts
         )

  nil; pair

getAllowance :: Entrypoint GetAllowanceParams Storage
getAllowance = do
  getField #ownerSpender; toField #owner; dip swap; getOwnersAccount
  stackType @[Account, Storage, GetAllowanceParams]

  toField #approvals; duupX @3; toField #ownerSpender; toField #spender; get
  ifNone (do push [mt|spender does not have an approval for this owner.|]; failWith) nop
  stackType @[Natural, Storage, GetAllowanceParams]

  dip (do swap; toField #contr; amount)
  transferTokens
  dip nil; cons; pair

getBalance :: Entrypoint GetBalanceParams Storage
getBalance = do
  -- lookup the owner's balance
  getField #owner; dip swap; getOwnersAccount; toField #balance

  -- transfer balance data to contr
  dip (do swap; toField #contr; amount);
  transferTokens
  dip nil; cons; pair

getTotalSupply :: Entrypoint GetTotalSupplyParams Storage
getTotalSupply = do
  toField #contr; amount
  duupX @3; stToField #totalSupply
  transferTokens
  dip nil; cons; pair
