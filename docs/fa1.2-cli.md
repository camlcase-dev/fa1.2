# Using the FA1.2 contract with tezos-client

This document will show you how to originate [fa1.2.tz](../morley/fa1.2.tz)
and interact with its entrypoints on the testnet.

You need the following:

- the tezos-client tool with a connection to a node on the testnet.
- an address you can sign with that has XTZ. The Tezos faucet can provide you some.
- a name, symbol, decimal placement and token amount for the FA1.2 contract you will originate.

## Originate an FA1.2 Token Contract

Lookup the address of the account with which you want to originate the 
FA1.2 token and give ownership of the coins to.

```bash
$ tezos-client list known addresses

simon: tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3 (unencrypted sk known)
bob: tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i (unencrypted sk known)
alice: tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn (unencrypted sk known)
```

I have three accounts on the testnet. I am going to originate a token with
1,000,000 tokens and with 8 decimal places. 
I will place all of them in Alice's custody. Copy the command 
below and replace `alice` with the name of your account and replace
`"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"` with your account's address. You 
can change the amount of tokens to whatever number you like. Make sure the two 
numbers match or there will be untransferrable tokens in the contract.

```bash
tezos-client originate contract tezosGold \
             transferring 0 from alice \
             running path/to/fa1.2.tz \
             --init 'Pair {Elt "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair 100000000000000 {})} 100000000000000' \ 
             --burn-cap 5
```

If you want to originate another contract stored as `tezosGold` in your local 
machine, you can overwrite it by adding `--force` to the end of the command. 
Just remember that it will disassociate the old contract address from 
the symbol, but that contract will still exist on the Tezos blockchain.

Now Alice can transfer Tezos Gold tokens to another account. In the command 
below, the first account address is the owner Alice and the second one is the 
to address Simon. The numerical value is the number of Tezos Gold tokens that 
Alice will send to Simon.

```bash
tezos-client transfer 0 \ 
             from alice \
             to tezosGold \
             --entrypoint 'transfer' \
             --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 100)' \
             --burn-cap 1
```

You can also allow other addresses to spend FA1.2 on your behalf by approving an
allowance that they can spend for you. 

```bash
tezos-client transfer 0 \ 
             from alice \
             to tezosGold 
             --entrypoint 'approve' \
             --arg 'Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 200' \
             --burn-cap 1
```

Now Simon can transfer up to 200 of Alice's FA1.2 for someone else. This is 
useful for interoperating with Dapps like Dexter. According to the FA1.2 standard,
if an allowance is zero it can be changed to a non-zero allownace and if it
is non-zero it can only be change to zero.

Finally, you can check the Tezos Gold balance of each account.

```bash
tezos-client get big map value \ 
             for '"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"' \
             of type 'address' in tezosGold
```
